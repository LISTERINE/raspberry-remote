Raspberry Remote
================

Hardware and software guide for building, configuring, and running an IR remote capture and playback tool with a raspberry Pi.

# Prep

### Parts:

Required:
* Raspberry Pi (I'm lazy and prefer headers and wifi) https://www.adafruit.com/product/3708
* Power https://www.adafruit.com/product/1995

Option 1 - DIY:
* PN2222 transistors https://www.adafruit.com/product/756
* IR Receiver Sensor - TSOP38238 https://www.adafruit.com/product/157
* IR led (one https://www.adafruit.com/product/387) or (many https://www.adafruit.com/product/388
* Resistors https://www.adafruit.com/product/2780
* Jumper wire https://www.adafruit.com/product/1954
* Breadboard https://www.adafruit.com/product/64
* Optional PCB:  https://oshpark.com/shared_projects/hAfniiW7

Option 2 - Prefab:
* Transceiver shield https://www.amazon.com/IR-Remote-Control-Transceiver-Raspberry/dp/B0713SK7RJ/ref=sr_1_1?s=industrial&ie=UTF8&qid=1519135386&sr=1-1&keywords=Transceiver+Shield

Optional:
* LEDs (assist with testing) https://www.adafruit.com/product/298
* Case https://www.adafruit.com/product/3252
* Raspberry proto bonnet https://www.adafruit.com/product/3203

### Process outline:

1. Wire components
2. Update Pi
3. Test components
4. Install, configure, and test LIRC
5. Record IR
6. Test Recording
7. Send IR


# Run

### Wire components
# TODO

### Update Pi

This section assumes that you are running raspbian.

These steps will update your package repositories, packages, and firmware.
The Raspberry Pi now updates its firmware through apt-get without rpi-update.

```shell
sudo apt-get update
sudo apt-get dist-upgrade
```


### Install, configure, and test LIRC

If using the prefab option, the gpio_in_pin is 18 and the gpio_out_pin is 17.
Just substitute these as you go along.

Install LIRC (Linux Infrared Remote Control):
```shell
sudo apt-get install lirc
```

Add these lines to /etc/modules:
```shell
lirc_dev
lirc_rpi gpio_in_pin=18 gpio_out_pin=17
```

Your /etc/lirc/hardware.conf file should look like this:
```shell
# /etc/lirc/hardware.conf
#
# Arguments which will be used when launching lircd
LIRCD_ARGS="--uinput"

#Don't start lircmd even if there seems to be a good config file
#START_LIRCMD=false

#Don't start irexec, even if a good config file seems to exist.
#START_IREXEC=false

#Try to load appropriate kernel modules
LOAD_MODULES=true

# Run "lircd --driver=help" for a list of supported drivers.
DRIVER="default"
# usually /dev/lirc0 is the correct setting for systems using udev
DEVICE="/dev/lirc0"
MODULES="lirc_rpi"

# Default configuration files for your hardware if any
LIRCD_CONF=""
LIRCMD_CONF=""
```

Update the dtoverlay line in in /boot/config.txt to look like this:
```shell
dtoverlay=lirc-rpi,gpio_in_pin=18,gpio_out_pin=17
```

Restart lirc and check that it's running:
```shell
sudo /etc/init.d/lirc stop
sudo /etc/init.d/lirc start
sudo /etc/init.d/lirc status
```

Reboot for all changes to take effect
```shell
sudo reboot
```

Test LIRC driver.
There are certain operations that require the LIRC daemon to be shut off. Keep that in mind as it can help troubleshoot things later.
Stop the LIRC daemon and use mode2 to read from the sensor, then point your remote at the IR sensor and press a button. You should start seeing pulses and spaces.
```shell
sudo /etc/init.d/lircd stop
mode2 -d /dev/lirc0
space 315479
pulse 1122
space 298997
pulse 872
space 174001
pulse 807
```



### Record IR
# TODO

### Test Recording
# TODO

### Send IR
# TODO
